#!/bin/sh

set -ex
git push
yarn build
rsync --progress --update -avz \
  dist/* root@149.28.188.36:/var/www/brandon.presley.nz
