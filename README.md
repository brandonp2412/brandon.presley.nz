# brandon.presley.nz

This is the source code for the website [brandon.presley.nz](https://brandon.presley.nz).

# Dependencies

- Node.js https://nodejs.org/en/

# Installing

```
yarn install
```

# Running in Development

```
yarn dev
```

# Building for Production

```
yarn build
```

# Relevant Documentation

- Svelte https://svelte.dev/docs
- TypeScript https://www.typescriptlang.org/docs/
- Web https://developer.mozilla.org/en-US/docs/Web
